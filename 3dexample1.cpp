//g++ -lSDL2 3dexample1.cpp

#include "lg.hpp"

#include <SDL2/SDL.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/transform2.hpp>

class SDLLG : public LG::Context
{
public:
    SDL_Window* window;
    SDL_Renderer* renderer;
    uint8_t cr,cg,cb;
	bool laser_on = false;

    SDLLG()
    {
        SDL_Init(SDL_INIT_VIDEO);
        SDL_CreateWindowAndRenderer(256,256,0,&window, &renderer);
    }
    ~SDLLG()
    {
        SDL_DestroyWindow(window);
        SDL_DestroyRenderer(renderer);
        SDL_Quit();
    }
    virtual void laserColor(const uint8_t r, const uint8_t g, const uint8_t b)
    {
        cr = r;
        cb = b;
        cg = g;
        SDL_SetRenderDrawColor(renderer, cr,cg,cb,255);
    }
    virtual void laserMove(const uint32_t x, const uint32_t y)
    {
        static uint32_t px, py;
        if(laser_on) { SDL_RenderDrawLine(renderer, px,py, x,y); }
        px = x;
        py = y;
    }
    virtual void laserOn()
    {
		laser_on = true;
        SDL_SetRenderDrawColor(renderer, cr,cg,cb,255);
    }
    virtual void laserOff()
    {
		laser_on = false;
        SDL_SetRenderDrawColor(renderer, 255,0,0,255);
    }
    virtual void clear()
    {
        SDL_SetRenderDrawColor(renderer, 0,0,0,255);
        SDL_RenderClear(renderer);
    }
    virtual void present()
    {
        SDL_RenderPresent(renderer);
    }

};

class FFT : public LG::VertexFunction
{
public:
    FFT()
    {
        //pj = glm::ortho(0.0f,255.0f,255.0f,0.0f);
        pj = glm::perspective(65.0f, 1.0f, 0.2f, 1000.0f);

    }

    virtual glm::vec4 operator() (const glm::vec4& in)
    {
        static float ang = 0.0f;
        mv = glm::translate(glm::mat4(1.0f), glm::vec3(2.0f * cos(ang),0.4f,-2.3f) );
        mv = glm::rotate(mv, ang, glm::vec3(1.0f,0.4f,1.0f));
        ang+=0.000004f;
        return pj * mv * in;
    }

    glm::mat4 mv, pj;
};


int main(int argc, char** argv)
{

    FFT *fft = new FFT;

    glm::vec3 cube_vertices[] =
    {
        glm::vec3{-1.0f,-1.0f,-1.0f},
        glm::vec3{1.0f,-1.0f,-1.0f},
        glm::vec3{1.0f,-1.0f,1.0f},
        glm::vec3{-1.0f,-1.0f,1.0f},

        glm::vec3{-1.0f,1.0f,-1.0f},
        glm::vec3{-1.0f,1.0f,1.0f},
        glm::vec3{1.0f,1.0f,1.0f},
        glm::vec3{1.0f,1.0f,-1.0f}
    };

    uint32_t cube_elements[] =
    {
        0,1,2,3,0,4,5,3,2,6,5,3,5,4,7,6,5,3,2,1,7,6,2,1,0,4,7
    };


    SDLLG c;

    c.setVertexFunction(fft);
    c.setViewPort(255,255);

    SDL_Event e;
    //For tracking if we want to quit
    bool quit = false;
    while (!quit)
    {
        //Read any events that occured, for now we'll just quit if any event occurs
        while (SDL_PollEvent(&e))
        {
            //If user closes the window
            if (e.type == SDL_QUIT)
            {
                quit = true;
            }

        }
        //Rendering
        c.clear();
        c.VertexPointer(3, cube_vertices);
        c.IndexPointer(cube_elements);
        c.DrawElements(LG::DrawType::Line_Strip, 27);

        c.present();
    }

    return 0;
}
