#ifndef LASERDRIVER_h
#define LASERDRIVER_h

#include <wiringPi.h>

class LaserDriver
{

public:
    void lSet( const unsigned char x, const unsigned char y )
    {
		setGalvoX(true);
        setDataBus( x );

        writeData();

		setGalvoX(false);
        setDataBus( y );

        writeData();
    }

    void lOn()
    {
        digitalWrite(RED_pin, HIGH);
        digitalWrite(GREEN_pin, HIGH);
        digitalWrite(BLUE_pin, HIGH);
    }

    void lOff()
    {
        digitalWrite(RED_pin, LOW);
        digitalWrite(GREEN_pin, LOW);
        digitalWrite(BLUE_pin, LOW);
    }

    void lColor(const unsigned char r, const unsigned char g, const unsigned char b)
    {
        if(r)
        {
            digitalWrite(RED_pin, HIGH);
        }
        else
        {
            digitalWrite(RED_pin, LOW);
        }

        if(g)
        {
            digitalWrite(GREEN_pin, HIGH);
        }
        else
        {
            digitalWrite(GREEN_pin, LOW);
        }

        if(b)
        {
            digitalWrite(BLUE_pin, HIGH);
        }
        else
        {
            digitalWrite(BLUE_pin, LOW);
        }
    }

    LaserDriver()
    {

        wiringPiSetup();

        D_pin[0] = 8;
        D_pin[1] = 9;
        D_pin[2] = 7;
        D_pin[3] = 15;
        D_pin[4] = 16;
        D_pin[5] = 0;
        D_pin[6] = 1;
        D_pin[7] = 2;

        WR_pin = 4;
        SEL_pin = 3;

        RED_pin = 25;
        GREEN_pin = 24;
        BLUE_pin = 23;

        for (int i = 0; i < 40; ++i)
        {
            pinMode(i, OUTPUT);
        }
    }

    ~LaserDriver()
    {
        for(int i = 0; i < 40; ++i)
        {
            digitalWrite(i, LOW);
        }
    }

	void setGalvoX( const bool x )
    {
		if(x)
		{
            digitalWrite( SEL_pin, LOW );
		}
		else
		{
            digitalWrite( SEL_pin, HIGH );
		}

    }

    void setDataBus( const unsigned char pos )
    {
        unsigned char temp;

		for(int i=7; i >= 0; i--)
        {
            temp = pos >> i;

            if ( temp & 1 )
			{
                digitalWrite( D_pin[i], HIGH );
			}
            else
			{
                digitalWrite( D_pin[i], LOW );
			}
        }
    }

    void writeData()
    {
        digitalWrite( WR_pin, LOW );
        digitalWrite( WR_pin, HIGH );
    }

protected:
    int D_pin[8];
    int WR_pin;
    int SEL_pin;

    int RED_pin;
    int BLUE_pin;
    int GREEN_pin;

};

#endif
