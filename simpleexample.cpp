//g++ -lSDL2 3dexample1.cpp

#include "lg.hpp"

#include <SDL2/SDL.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/transform2.hpp>

class SDLLG : public LG::Context
{
public:
    SDL_Window* window;
    SDL_Renderer* renderer;
    uint8_t cr,cg,cb;
	bool laser_on = false;

    SDLLG()
    {
        SDL_Init(SDL_INIT_VIDEO);
        SDL_CreateWindowAndRenderer(256,256,0,&window, &renderer);
    }
    ~SDLLG()
    {
        SDL_DestroyWindow(window);
        SDL_DestroyRenderer(renderer);
        SDL_Quit();
    }
    virtual void laserColor(const uint8_t r, const uint8_t g, const uint8_t b)
    {
        cr = r;
        cb = b;
        cg = g;
        SDL_SetRenderDrawColor(renderer, cr,cg,cb,255);
    }
    virtual void laserMove(const uint32_t x, const uint32_t y)
    {
        static uint32_t px, py;
        if(laser_on) { SDL_RenderDrawLine(renderer, px,py, x,y); }
        px = x;
        py = y;
    }
    virtual void laserOn()
    {
		laser_on = true;
        SDL_SetRenderDrawColor(renderer, cr,cg,cb,255);
    }
    virtual void laserOff()
    {
		laser_on = false;
        SDL_SetRenderDrawColor(renderer, 255,0,0,255);
    }
    virtual void clear()
    {
        SDL_SetRenderDrawColor(renderer, 0,0,0,255);
        SDL_RenderClear(renderer);
    }
    virtual void present()
    {
        SDL_RenderPresent(renderer);
    }

};

class FFT : public LG::VertexFunction
{
public:
    FFT()
    {
        pj = glm::ortho(0.0f,255.0f,255.0f,0.0f);
    }

    virtual glm::vec4 operator() (const glm::vec4& in)
    {
        return pj * mv * in;
    }

    glm::mat4 mv, pj;
};


int main(int argc, char** argv)
{

    FFT *fft = new FFT;

    glm::vec2 verts[]
    {
		{10.0f,10.0f},
		{20.0f,10.0f},
		{50.0f,50.0f},
		{75.0f,75.0f}
	};

    SDLLG c;

    c.setVertexFunction(fft);
    c.setViewPort(255,255);

    SDL_Event e;
    //For tracking if we want to quit
    bool quit = false;
    while (!quit)
    {
        //Read any events that occured, for now we'll just quit if any event occurs
        while (SDL_PollEvent(&e))
        {
            //If user closes the window
            if (e.type == SDL_QUIT)
            {
                quit = true;
            }

        }
        //Rendering
        c.clear();
        c.VertexPointer(2, verts);
        c.DrawArray(LG::DrawType::Lines, 0, 4);

        c.present();
    }

    return 0;
}
 
